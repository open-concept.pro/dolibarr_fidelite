Reward point
=========

This module goal is to provide reward point to final customer from adherent.

Licence
-------
GPLv3 or (at your option) any later version.

See COPYING for more information.

Other Licences
--------------
Uses Michel Fortin's PHP Markdown Licensed under BSD to display this README.
