<?php
/* Copyright (C) 2012	Florian HENRY 		<florian.henry@open-concept.pro>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file		fidelite/customer/index.php
 *	\ingroup	fidelite
 *	\brief		customer login pages
 */


$res = 0;
if ( ! $res && file_exists("../main.inc.php"))
	$res = @include("../main.inc.php");
if ( ! $res && file_exists("../../main.inc.php"))
	$res = @include("../../main.inc.php");
if ( ! $res && file_exists("../../../main.inc.php"))
	$res = @include("../../../main.inc.php");
if ( ! $res) die("Main include failed");


//require_once DOL_DOCUMENT_ROOT . "custom/mymodule/class/myclass.class.php";

// Load translation files required by the page
$langs->load("fidelite@fidelite");

// Get parameters
$id = GETPOST('id', 'int');
$action = GETPOST('action', 'alpha');

// Security check
if (!$user->rights->fidelite->customer->read) accessforbidden();


/*
 * ACTIONS
*
* Put here all code to do according to value of "action" parameter
*/

if ($action == 'add') {
	$myobject = new Skeleton_Class($db);
	$myobject->prop1 = $_POST["field1"];
	$myobject->prop2 = $_POST["field2"];
	$result = $myobject->create($user);
	if ($result > 0) {
		// Creation OK
	} {
		// Creation KO
		$mesg = $myobject->error;
	}
}


/*
 * VIEW
*
* Put here all code to build page
*/

llxHeader('',$langs->trans('Module190000Name'));

$form = new Form($db);

print 'index customer';


// End of page
llxFooter();
$db->close();
?>

