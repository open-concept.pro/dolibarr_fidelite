<?php
/* Copyright (C) 2001-2002	Rodolphe Quiedeville	<rodolphe@quiedeville.org>
 * Copyright (C) 2001-2002	Jean-Louis Bergamo		<jlb@j1b.org>
 * Copyright (C) 2006-2011	Laurent Destailleur		<eldy@users.sourceforge.net>
 * Copyright (C) 2012		Regis Houssin			<regis@dolibarr.fr>
 * Copyright (C) 2012		J. Fernando Lagrange 		<fernando@demo-tic.org>
 * Copyright (C) 2012	Florian HENRY	<florian.henry@open-concept.pro>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       fidelite/public/fidelite/new.php
 *	\ingroup    fidelite
 *	\brief      form to add a new customer
 *
 */


error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);


define("NOLOGIN",1);		// This means this output page does not require to be logged.
define("NOCSRFCHECK",1);	// We accept to go on this page from external web site.

// For MultiCompany module
$entity=(! empty($_GET['entity']) ? (int) $_GET['entity'] : (! empty($_POST['entity']) ? (int) $_POST['entity'] : 1));
if (is_int($entity))
{
	define("DOLENTITY", $entity);
}

$res = 0;
if ( ! $res && file_exists("../main.inc.php"))
	$res = @include("../main.inc.php");
if ( ! $res && file_exists("../../main.inc.php"))
	$res = @include("../../main.inc.php");
if ( ! $res && file_exists("../../../main.inc.php"))
	$res = @include("../../../main.inc.php");
if ( ! $res) die("Main include failed");

dol_include_once('/fidelite/class/dao_fidelite.class.php');

require_once DOL_DOCUMENT_ROOT.'/societe/class/societe.class.php';
require_once DOL_DOCUMENT_ROOT.'/contact/class/contact.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/user/class/user.class.php';

// Init vars
$errmsg='';
$num=0;
$error=0;
$backtopage=GETPOST('backtopage','alpha');
$action=GETPOST('action','alpha');

//Form Vars
$loginRFID = GETPOST('loginRFID','alpha');
$name = GETPOST('custname','alpha');
$firstname = GETPOST('custfirstname','alpha');
$civilite_id = GETPOST('civilite_id','alpha');
$address = GETPOST('custadress','alpha');
$zip = GETPOST('custzip','alpha');
$town = GETPOST('custtown','alpha');
$email = GETPOST('custemail','alpha');
$tel = GETPOST('custtel','alpha');
$mobile = GETPOST('custmobile','alpha');
$pass1=GETPOST('pass1');
$pass2=GETPOST('pass2');
$birthday=dol_mktime(0,0,0,GETPOST('birthmonth','int'),GETPOST('birthday','int'),GETPOST('birthyear','int'));

// Load translation files
$langs->load("errors");
$langs->load("companies");
$langs->load("install");
$langs->load("other");
$langs->load("fidelite@fidelite");

// Security check
if (empty($conf->fidelite->enabled)) accessforbidden('',1,1,1);


/**
 * Show header for new member
 *
 * @param 	string		$title				Title
 * @param 	string		$head				Head array
 * @param 	int    		$disablejs			More content into html header
 * @param 	int    		$disablehead		More content into html header
 * @param 	array  		$arrayofjs			Array of complementary js files
 * @param 	array  		$arrayofcss			Array of complementary css files
 * @return	void
 */
function llxHeaderVierge($title, $head="", $disablejs=0, $disablehead=0, $arrayofjs='', $arrayofcss='')
{
    global $user, $conf, $langs, $mysoc;
    top_htmlhead($head, $title, $disablejs, $disablehead, $arrayofjs, $arrayofcss); // Show html headers
    print '<body id="mainbody" class="publicnewmemberform" style="margin-top: 10px;">';

    // Print logo
    $urllogo=DOL_URL_ROOT.'/theme/login_logo.png';

    if (! empty($mysoc->logo_small) && is_readable($conf->mycompany->dir_output.'/logos/thumbs/'.$mysoc->logo_small))
    {
        $urllogo=DOL_URL_ROOT.'/viewimage.php?cache=1&amp;modulepart=companylogo&amp;file='.urlencode('thumbs/'.$mysoc->logo_small);
    }
    elseif (! empty($mysoc->logo) && is_readable($conf->mycompany->dir_output.'/logos/'.$mysoc->logo))
    {
        $urllogo=DOL_URL_ROOT.'/viewimage.php?cache=1&amp;modulepart=companylogo&amp;file='.urlencode($mysoc->logo);
        $width=128;
    }
    elseif (is_readable(DOL_DOCUMENT_ROOT.'/theme/dolibarr_logo.png'))
    {
        $urllogo=DOL_URL_ROOT.'/theme/dolibarr_logo.png';
    }
    print '<center>';
    print '<img alt="Logo" id="logosubscribe" title="" src="'.$urllogo.'" />';
    print '</center><br>';

    print '<div style="margin-left: 50px; margin-right: 50px;">';
}

/**
 * Show footer for new member
 *
 * @return	void
 */
function llxFooterVierge()
{
	
    print '</div>';
    
    dol_htmloutput_events();
    
    print "</body>\n";
    print "</html>\n";
}



/*
 * Actions
 */

// Action called when page is submited
if ($action == 'add')
{		
	$error = 0;
	

	//Test RFID not already used
	if(empty($loginRFID))
	{
		$error++;
		$errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("FidRFID"))."<br>\n";
	}
	$sql = "SELECT barcode FROM ".MAIN_DB_PREFIX."societe WHERE barcode='".$db->escape($loginRFID)."'";
	$result = $db->query($sql);
	if ($result)
	{
		$num = $db->num_rows($result);
	}
	if ($num !=0)
	{
		$error++;
		$langs->load("errors");
		$errmsg .= $langs->trans("FidErrorRFIDAlreadyExists")."<br>\n";
	}else {
		//TODO : remove because spécific guilde
		//Check if RFID is in guilde database
		
	}
	if (empty($pass1) || empty($pass2) || $pass1!=$pass2)
	{
		$error++;
		$langs->load("errors");
		$errmsg .= $langs->trans("ErrorPasswordsMustMatch")."<br>\n";
	}
	if (empty($email))
	{
		$error++;
		$errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("EMail"))."<br>\n";
	}elseif (! isValidEmail($email)) {
		$errmsg .= $langs->trans("ErrorBadEMail",$email)."<br>\n";
	}
    if (empty($name))
    {
        $error++;
        $errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("Lastname"))."<br>\n";
    }
    if (empty($civilite_id) || $civilite_id == -1)
    {
    	$error++;
    	$errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("UserTitle"))."<br>\n";
    }
    if (empty($firstname))
    {
        $error++;
        $errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("Firstname"))."<br>\n";
    }
   
    if (empty($birthday))
    {
    	$error++;
    	$errmsg .= $langs->trans("ErrorFieldRequired",$langs->transnoentitiesnoconv("DateToBirth"))."<br>\n";
    }
    

    if (! $error)
    {
    	
    	$soc = new Societe($db);
		
    	$soc->code_client = -1; //Generate new auto customer code
    	$soc->name=$name.' '.$firstname;
    	$soc->address = $address;
    	$soc->zip = $zip;
    	$soc->town = $town;
    	$soc->phone = $tel;
    	$soc->email = $email;
    	$soc->typent_id = 8; //For thirdparty is personnal
    	$soc->status= 1 ; //Customer is active

        $result=$soc->create($user->id);
        if ($result<0) {
        	$error++;
        	setEventMessage($soc->error,'errors');
        }
    }
        
	if (! $error)	{
		
		$contact = new Contact($db);
		
        $contact->civilite_id		= $civilite_id;
        $contact->name				= $name;
        $contact->firstname			= $firstname;
        $contact->address			= $address;
        $contact->zip				= $zip;
        $contact->town				= $town;
        $contact->socid				= $soc->id;	// fk_soc
        $contact->status			= 1;
        $contact->email				= $email;
        $contact->phone_pro			= $tel;
        $contact->phone_mobile		= $mobile;
        $contact->priv				= 0;
        $contact->bithday			= $birthday;
        
        $contactid=$contact->create($user->id);
        if ($contactid<0) {
        	$error++;
        	setEventMessage($contact->error,'errors');
        }
        
	}
	
	if (! $error) {
		$nuser = new User($db);
		$nuser->pass=$pass1;
		$userid = $nuser->create_from_contact($contact,$loginRFID);
		if ($userid<=0) {
			$error++;
			setEventMessage($nuser->error,'errors');
		}else {
			$nuser->id=$userid;
		}
	}
	
	if (! $error)	{
	
		$fid = new DaoFidelite($db);
		
		$fid->RFID_key		= $loginRFID;
		$fid->fk_socpeople	= $contactid;
		$fid->reward_point	= 0;
		$fid->active		= 1;

		$result=$fid->create($nuser);
		if ($result<0) {
			$error++;
			setEventMessage($fid->error,'errors');
		}
	
	}
	
	if ( ! $error)
	{
		$urlback = $_SERVER["PHP_SELF"]."?action=added";
		dol_syslog("Customers ".$soc->ref_int." was created, we redirect to ".$urlback);
		Header("Location: ".$urlback);
		exit;
	}else {
		setEventMessage($errmsg,'errors');
	}
}

// Action called after a submited was send and member created succesfully
// If MEMBER_URL_REDIRECT_SUBSCRIPTION is set to url we never go here because a redirect was done to this url.
// backtopage parameter with an url was set on member submit page, we never go here because a redirect was done to this url.
if ($action == 'added')
{
    llxHeaderVierge($langs->trans("FidNewCust"));
    setEventMessage($langs->trans("FidCustCreated"),'mesgs');
    // Si on a pas ete redirige
    print '<br>';
    print '<center>';
    print $langs->trans("FidNewCustbyWeb");
    print '</center>';
    
    print '<br>';
    print '<center>';
    print '<a href="'.DOL_URL_ROOT.'">'.$langs->trans("FidLoginPage").'</a>';
    print '</center>';

    llxFooterVierge();
    exit;
}



/*
 * View
 */

$form = new Form($db);
$formcompany = new FormCompany($db);

llxHeaderVierge($langs->trans("FidNewCust"));


print_titre($langs->trans("FidNewCust"));

print '<div align="center">';

print '<br>'.$langs->trans("FieldsWithAreMandatory",'*').'<br>';

// Print form
print '<form action="'.$_SERVER["PHP_SELF"].'" method="POST" name="newmember">'."\n";
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'" / >';
print '<input type="hidden" name="entity" value="'.$entity.'" />';
print '<input type="hidden" name="action" value="add" />';

print '<div id="divsubscribe">';
print '<table class="border" summary="form to subscribe" id="tablesubscribe">'."\n";

// Civility
print '<tr><td>'.$langs->trans('UserTitle').'<FONT COLOR="red">*</FONT></td><td>';
print $formcompany->select_civility($civilite_id,'civilite_id').'</td></tr>'."\n";
// Lastname
print '<tr><td>'.$langs->trans("Lastname").' <FONT COLOR="red">*</FONT></td><td><input type="text" name="custname" size="40" value="'.dol_escape_htmltag($name).'"></td></tr>'."\n";
// Firstname
print '<tr><td>'.$langs->trans("Firstname").' <FONT COLOR="red">*</FONT></td><td><input type="text" name="custfirstname" size="40" value="'.dol_escape_htmltag($firstname).'"></td></tr>'."\n";
// Address
print '<tr><td>'.$langs->trans("Address").'</td><td>'."\n";
print '<textarea name="custadress" id="custadress" wrap="soft" cols="40" rows="'.ROWS_3.'">'.dol_escape_htmltag($address).'</textarea></td></tr>'."\n";
// Zip / Town
print '<tr><td>'.$langs->trans('Zip').' / '.$langs->trans('Town').'<FONT COLOR="red">*</FONT></td><td>';
print $formcompany->select_ziptown($zip, 'custzip', array('custtown','selectcountry_id','state_id'), 6, 1);
print ' / ';
print $formcompany->select_ziptown($town, 'custtown', array('zipcode','selectcountry_id','state_id'), 0, 1);
print '</td></tr>';
// EMail
print '<tr><td>'.$langs->trans("Email").' <FONT COLOR="red">*</FONT></td><td><input type="text" name="custemail" size="40" value="'.dol_escape_htmltag($email).'"></td></tr>'."\n";
// Login
print '<tr><td>'.$langs->trans("FidRFID").' <FONT COLOR="red">*</FONT></td><td><input type="text" name="loginRFID" size="20" value="'.dol_escape_htmltag($loginRFID).'"></td></tr>'."\n";
print '<tr><td>'.$langs->trans("Password").' <FONT COLOR="red">*</FONT></td><td><input type="password" name="pass1" size="20" value="'.$pass1.'"></td></tr>'."\n";
print '<tr><td>'.$langs->trans("PasswordAgain").' <FONT COLOR="red">*</FONT></td><td><input type="password" name="pass2" size="20" value="'.$pass2.'"></td></tr>'."\n";
// Birthday
print '<tr><td>'.$langs->trans("DateToBirth").'<FONT COLOR="red">*</FONT></td><td>';
print $form->select_date($birthday,'birth',0,0,1,"newmember");
print '</td></tr>'."\n";
print "</table>\n";

// Save
print '<br><center>';
print '<input type="submit" value="'.$langs->trans("Save").'" id="submitsave" class="button">';
if (! empty($backtopage))
{
    print ' &nbsp; &nbsp; <input type="submit" value="'.$langs->trans("Cancel").'" id="submitcancel" class="button">';
}
print '</center>';

print "<br></div></form>\n";
print '</div>';

print
llxFooterVierge();

$db->close();
?>
