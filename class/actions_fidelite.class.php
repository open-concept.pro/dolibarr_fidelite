<?php
/* Copyright (C) 2012	Florian HENRY 		<florian.henry@open-concept.pro>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 	\file 		/fidelite/class/actions_fidelite.class.php
 *	\ingroup    fidelite
 *	\brief      File of class to manage fidelite
 */

dol_include_once('/fidelite/class/dao_fidelite.class.php');


/**
 * 	\class 		ActionsFidelite
 *	\brief      Class to manage fidelite
 */
class ActionsFidelite
{
	var $db;
	var $dao;

	var $error;
	var $errors=array();


	/**
	 *  Constructor
	 *
	 *	@param	DoliDB	$db			Database handler
	*/
	function __construct($db)
	{
		$this->db = $db ;
		$this->error = 0;
		$this->errors = array();

	}
	
	/**
	 *  Constructor
	 *
	 *	@param	$entity		int			User entity
	 *	@return array	array of options to display on ogin page
	 */
	function getLoginPageOptions($entity) {
		global $langs;
		
		$langs->load("fidelite@fidelite");
		
		$htmltable='<tr><td colspan="2">&nbsp;</td></tr>';
		$htmltable.='<tr>';
		$htmltable.='<td align="left">';
		$htmltable.=$langs->trans('FidCreateCustomerDesc');
		$htmltable.='</td>';
		$htmltable.='<td align="center">';
		$htmltable.='<a class="button" href="'.dol_buildpath('/fidelite/public/fidelite/new.php',1).'">'.$langs->trans('FidCreateCustomerShort').'</a>';
		$htmltable.='</td>';
		$htmltable.='</tr>';
		
		$ret = array();
		$ret['options'] = array('table'=>$htmltable);
		
		return $ret;
	}
}