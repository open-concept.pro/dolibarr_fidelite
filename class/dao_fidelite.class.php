<?php
/* Copyright (C) 2012	Florian Henry	<florian.henry@open-concept.pro>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

/**
 *  \file       fidelite/class/dao_fidelite.class.php
*  \ingroup    fidelite
*  \brief      Manage objects
*/

require_once(DOL_DOCUMENT_ROOT ."/core/class/commonobject.class.php");

/**
 *	Fidelite Class
*/
class DaoFidelite extends CommonObject
{
	var $db;
	var $error;
	var $errors=array();
	var $element='fidelite';
	var $table_element='fid_device';

	var $id;
	var $fk_socpeople;
	var $entity;
	var $RFID_key;
	var $reward_point;
	var $active;
	var $fk_user_author;
	var $datec='';
	var $fk_user_mod;
	var $tms='';
	var $lines=array();

	/**
	 *  Constructor
	 *
	 *  @param	DoliDb		$db      Database handler
	*/
	function __construct($DB)
	{
		$this->db = $DB;
		return 1;
	}
	
	
	/**
	 *  Create object into database
	 *
	 *  @param	User	$user        User that creates
	 *  @param  int		$notrigger   0=launch triggers after, 1=disable triggers
	 *  @return int      		   	 <0 if KO, Id of created object if OK
	 */
	function create($user, $notrigger=0)
	{
		global $conf, $langs;
		$error=0;
	
		// Clean parameters
	
		if (isset($this->RFID_key)) $this->RFID_key=trim($this->RFID_key);
		if (isset($this->reward_point)) $this->reward_point=trim($this->reward_point);
		if (isset($this->active)) $this->active=trim($this->active);
	
		// Check parameters
		// Put here code to add control on parameters values
	
		// Insert request
		$sql = "INSERT INTO ".MAIN_DB_PREFIX."fid_device(";
	
		$sql.= "entity,";
		$sql.= "RFID_key,";
		$sql.= "reward_point,";
		$sql.= "active,";
		$sql.= "fk_user_author,";
		$sql.= "datec,";
		$sql.= "fk_user_mod";
	
	
		$sql.= ") VALUES (";
	
		$sql.= "'".$conf->entity."'".",";
		$sql.= " ".(! isset($this->RFID_key)?'NULL':"'".$this->db->escape($this->RFID_key)."'").",";
		$sql.= " ".(! isset($this->reward_point)?'NULL':"'".$this->reward_point."'").",";
		$sql.= " ".(! isset($this->active)?'NULL':"'".$this->active."'").",";
		$sql.= " ".$user->id.",";
		$sql.= " ".$this->db->idate(dol_now()).",";
		$sql.= " ".$user->id;
		$sql.= ")";
	
		$this->db->begin();
	
		dol_syslog(get_class($this)."::create sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if (! $resql) { $error++; $this->errors[]="Error ".$this->db->lasterror(); }
	
		if (! $error)
		{
			$this->id = $this->db->last_insert_id(MAIN_DB_PREFIX."fid_device");
	
			$result = $this->add_object_linked('contact',$this->fk_socpeople);
			if ($result<=0) {
				$error++;
			}
		}
	
		// Commit or rollback
		if ($error)
		{
			foreach($this->errors as $errmsg)
			{
				dol_syslog(get_class($this)."::create ".$errmsg, LOG_ERR);
				$this->error.=($this->error?', '.$errmsg:$errmsg);
			}
			$this->db->rollback();
			return -1*$error;
		}
		else
		{
			$this->db->commit();
			return $this->id;
		}
	}
	
	
	/**
	 *  Load object in memory from the database
	 *
	 *  @param	int		$id    Id object
	 *  @return int          	<0 if KO, >0 if OK
	 */
	function fetch($id)
	{
		global $langs;
		$sql = "SELECT";
		$sql.= " t.rowid,";
	
		$sql.= " t.entity,";
		$sql.= " t.RFID_key,";
		$sql.= " t.reward_point,";
		$sql.= " t.active,";
		$sql.= " t.fk_user_author,";
		$sql.= " t.datec,";
		$sql.= " t.fk_user_mod,";
		$sql.= " t.tms";
	
	
		$sql.= " FROM ".MAIN_DB_PREFIX."fid_device as t";
		$sql.= " WHERE t.rowid = ".$id;
		$sql.= " AND t.entity IN (".getEntity('fidelite').")";
		
	
		dol_syslog(get_class($this)."::fetch sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql)
		{
			if ($this->db->num_rows($resql))
			{
				$obj = $this->db->fetch_object($resql);
	
				$this->id    = $obj->rowid;
	
				$this->entity = $obj->entity;
				$this->RFID_key = $obj->RFID_key;
				$this->reward_point = $obj->reward_point;
				$this->active = $obj->active;
				$this->fk_user_author = $obj->fk_user_author;
				$this->datec = $this->db->jdate($obj->datec);
				$this->fk_user_mod = $obj->fk_user_mod;
				$this->tms = $this->db->jdate($obj->tms);
	
	
			}
			$this->db->free($resql);
	
			return 1;
		}
		else
		{
			$this->error="Error ".$this->db->lasterror();
			dol_syslog(get_class($this)."::fetch ".$this->error, LOG_ERR);
			return -1;
		}
	}
	
	/**
	 *  Load object in memory from the database
	 *
	 *  @param  $by_entity	int	 1 is yes, 0 is no	
	 *  @return int          	<0 if KO, >0 if OK
	 */
	function fetch_all($by_entity=1)
	{
		global $langs;
		$sql = "SELECT";
		$sql.= " t.rowid,";
		$sql.= " t.entity,";
		$sql.= " t.RFID_key,";
		$sql.= " t.reward_point,";
		$sql.= " t.active,";
		$sql.= " t.fk_user_author,";
		$sql.= " t.datec,";
		$sql.= " t.fk_user_mod,";
		$sql.= " t.tms";
		$sql.= " FROM ".MAIN_DB_PREFIX."fid_device as t";
		if ($by_entity) {
			$sql.= " AND t.entity IN (".getEntity('fidelite').")";
		}
	
		dol_syslog(get_class($this)."::fetch_all sql=".$sql, LOG_DEBUG);
		$resql=$this->db->query($sql);
		if ($resql)
		{
			$this->lines = array();
			
			$i=0;
			$num = $this->db->num_rows($resql);
			
			while ($i<$num)
			{
				$line = new DaoFideliteLine();
				$obj = $this->db->fetch_object($resql);
	
				$line->id    = $obj->rowid;
	
				$line->entity = $obj->entity;
				$line->RFID_key = $obj->RFID_key;
				$line->reward_point = $obj->reward_point;
				$line->active = $obj->active;
				$line->fk_user_author = $obj->fk_user_author;
				$line->datec = $this->db->jdate($obj->datec);
				$line->fk_user_mod = $obj->fk_user_mod;
				$line->tms = $this->db->jdate($obj->tms);
	
				$this->lines[$i]=$line;
				
				$i++;
	
			}
			$this->db->free($resql);
	
			return 1;
		}
		else
		{
			$this->error="Error ".$this->db->lasterror();
			dol_syslog(get_class($this)."::fetch_all ".$this->error, LOG_ERR);
			return -1;
		}
	}
	
	
	/**
	 *  Update object into database
	 *
	 *  @param	User	$user        User that modifies
	 *  @param  int		$notrigger	 0=launch triggers after, 1=disable triggers
	 *  @return int     		   	 <0 if KO, >0 if OK
	 */
	function update($user=0, $notrigger=0)
	{
		global $conf, $langs;
		$error=0;
	
		// Clean parameters
		if (isset($this->RFID_key)) $this->RFID_key=trim($this->RFID_key);
		if (isset($this->reward_point)) $this->reward_point=trim($this->reward_point);
		if (isset($this->active)) $this->active=trim($this->active);	
	
		// Check parameters
		// Put here code to add a control on parameters values
	
		// Update request
		$sql = "UPDATE ".MAIN_DB_PREFIX."fid_device SET";
	
		$sql.= " entity=".$conf->entity.",";
		$sql.= " RFID_key=".(isset($this->RFID_key)?"'".$this->db->escape($this->RFID_key)."'":"null").",";
		$sql.= " reward_point=".(isset($this->reward_point)?$this->reward_point:"null").",";
		$sql.= " active=".(isset($this->active)?$this->active:"null").",";
		$sql.= " fk_user_mod=".$user->id;
	
	
		$sql.= " WHERE rowid=".$this->id;
	
		$this->db->begin();
	
		dol_syslog(get_class($this)."::update sql=".$sql, LOG_DEBUG);
		$resql = $this->db->query($sql);
		if (! $resql) { $error++; $this->errors[]="Error ".$this->db->lasterror(); }
	
		if (! $error)
		{
			if (! $notrigger)
			{
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action calls a trigger.
	
				//// Call triggers
				//include_once DOL_DOCUMENT_ROOT . '/core/class/interfaces.class.php';
				//$interface=new Interfaces($this->db);
				//$result=$interface->run_triggers('MYOBJECT_MODIFY',$this,$user,$langs,$conf);
				//if ($result < 0) { $error++; $this->errors=$interface->errors; }
				//// End call triggers
			}
		}
	
		// Commit or rollback
		if ($error)
		{
			foreach($this->errors as $errmsg)
			{
				dol_syslog(get_class($this)."::update ".$errmsg, LOG_ERR);
				$this->error.=($this->error?', '.$errmsg:$errmsg);
			}
			$this->db->rollback();
			return -1*$error;
		}
		else
		{
			$this->db->commit();
			return 1;
		}
	}
	
	
	/**
	 *  Delete object in database
	 *
	 *	@param  User	$user        User that deletes
	 *  @param  int		$notrigger	 0=launch triggers after, 1=disable triggers
	 *  @return	int					 <0 if KO, >0 if OK
	 */
	function delete($user, $notrigger=0)
	{
		global $conf, $langs;
		$error=0;
	
		$this->db->begin();
	
		if (! $error)
		{
			if (! $notrigger)
			{
				// Uncomment this and change MYOBJECT to your own tag if you
				// want this action calls a trigger.
	
				//// Call triggers
				//include_once DOL_DOCUMENT_ROOT . '/core/class/interfaces.class.php';
				//$interface=new Interfaces($this->db);
				//$result=$interface->run_triggers('MYOBJECT_DELETE',$this,$user,$langs,$conf);
				//if ($result < 0) { $error++; $this->errors=$interface->errors; }
				//// End call triggers
			}
		}
	
		if (! $error)
		{
			$sql = "DELETE FROM ".MAIN_DB_PREFIX."fid_device";
			$sql.= " WHERE rowid=".$this->id;
	
			dol_syslog(get_class($this)."::delete sql=".$sql);
			$resql = $this->db->query($sql);
			if (! $resql) { $error++; $this->errors[]="Error ".$this->db->lasterror(); }
		}
	
		// Commit or rollback
		if ($error)
		{
			foreach($this->errors as $errmsg)
			{
				dol_syslog(get_class($this)."::delete ".$errmsg, LOG_ERR);
				$this->error.=($this->error?', '.$errmsg:$errmsg);
			}
			$this->db->rollback();
			return -1*$error;
		}
		else
		{
			$this->db->commit();
			return 1;
		}
	}
	
	
	/**
	 *	Initialise object with example values
	 *	Id must be 0 if object instance is a specimen
	 *
	 *	@return	void
	 */
	function initAsSpecimen()
	{
		$this->id=0;
	
		$this->entity='';
		$this->RFID_key='';
		$this->reward_point='';
		$this->active='';	
	
	}
	
}

/**
 *	Fidelite Lines Class
 */
class DaoFideliteLine {
	
	var $id;
	var $entity;
	var $RFID_key;
	var $reward_point;
	var $active;
	var $fk_user_author;
	var $datec='';
	var $fk_user_mod;
	var $tms='';
	
	/**
	 *  Constructor
	 */
	function __construct()
	{
		return 1;
	}
	
	
}